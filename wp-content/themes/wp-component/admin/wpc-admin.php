<?php
/**
 * Created by Aloïs Petit
 * url: www.alois-petit.fr
 *
 * @_00 - Remove links from WordPress Head
 * @_01 - Excerpt page in back office
 * @_02 - Wysiwyg editor
 * @_03 - Custom WordPress Login Logo
 * @_04 - Custom WordPress Admin Color Scheme
 * @_05 - Use your own external URL logo link
 * @_06 - Custom WordPress Footer
 * @_07 - Rename "posts" in admin menu bar
 * @_08 - Rename "posts" in admin menu bar
 * @_09 -  Add a news widget in WordPress Dashboard
 * @_10 -  Hiding widgets in WordPress Dashboard
 * @_11 -  Editor style
 *
 */

/* @_00 - Remove links from WordPress Head
/ ================================================== */
    remove_action( 'wp_head', 'feed_links_extra', 3 );              // Removes the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links', 2 );                    // Removes links to the general feeds: Post and Comment Feed
    remove_action( 'wp_head', 'rsd_link');                          // Removes the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link');                  // Removes the link to the Windows Live Writer manifest file.
    remove_action( 'wp_head', 'index_rel_link');                    // Removes the index link
    remove_action( 'wp_head', 'parent_post_rel_link');              // Removes the prev link
    remove_action( 'wp_head', 'start_post_rel_link');               // Removes the start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head');   // Removes the relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'wp_generator');                      // Removes the WordPress version i.e. - WordPress 2.8.4

/* @_01 - Excerpt page in back office
/ ================================================== */
    function wpc_excerpt_pages() {
        add_meta_box('postexcerpt', __("Extrait", DOMAIN), 'post_excerpt_meta_box', 'page', 'normal', 'core');
    }
    add_action( 'admin_menu', 'wpc_excerpt_pages' );

/* @_03 - Custom WordPress Login Logo
/ ================================================== */
    function login_css() {
        wp_enqueue_style( 'login_css', get_template_directory_uri() . '/admin/css/login.css' );
    }
    add_action('login_head', 'login_css');

/* @_04 - Custom WordPress Admin Color Scheme
/ ================================================== */
    function admin_css() {
        wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/admin/css/admin.css' );
    }
    add_action('admin_print_styles', 'admin_css' );

/* @_05 - Use your own external URL logo link
/ ================================================== */
    function wpc_url_login(){
        return "https://alois-petit.fr";
    }
    add_filter('login_headerurl', 'wpc_url_login');

/* @_06 - Custom WordPress Footer
/ ================================================== */
    function modify_footer_admin () {
        echo '&copy; ' . date( 'Y' ) . ' - <a href="https://alois-petit.fr" target="_blank">Aloïs Petit</a> - ';
        echo 'Powered by <a href="http://WordPress.org">WordPress</a> ' . get_bloginfo('version');
    }
    add_filter('admin_footer_text', 'modify_footer_admin');





/* @_07 - Rename "posts" in admin menu bar
/ ================================================== */
    function change_post_menu_label() {
        global $menu;
        global $submenu;
        $menu[5][0] = __('Actualités', DOMAIN);
        $submenu['edit.php'][5][0] = __("Actualités", DOMAIN);
        $submenu['edit.php'][10][0] = __("Ajouter", DOMAIN);
        echo '';
    }

    function change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = __("Actualités", DOMAIN);
        $labels->singular_name = __("Actualité", DOMAIN);
        $labels->add_new = __("Ajouter une actualité", DOMAIN);
        $labels->add_new_item = __("Ajouter une actualité", DOMAIN);
        $labels->edit_item = __("Modifier les actualités", DOMAIN);
        $labels->new_item = __("Actualité", DOMAIN);
        $labels->view_item = __("Voir l'actualité", DOMAIN);
        $labels->search_items = __("Rechercher une actualité", DOMAIN);
        $labels->not_found = __("Aucune actualité trouvée", DOMAIN);
        $labels->not_found_in_trash = __("Aucune actualité trouvée dans la corbeille", DOMAIN);
    }
    add_action( 'init', 'change_post_object_label' );
    add_action( 'admin_menu', 'change_post_menu_label' );

/* @_08 - Remove links from admin menu bar
/ ================================================== */
    function remove_menus(){
        //remove_menu_page( 'index.php' );                  // Dashboard
        //remove_menu_page( 'edit.php' );                   // Posts
        //remove_menu_page( 'upload.php' );                 // Media
        //remove_menu_page( 'edit.php?post_type=page' );    // Pages
        remove_menu_page( 'edit-comments.php' );            // Comments
        //remove_menu_page( 'themes.php' );                 // Appearance
        //remove_menu_page( 'plugins.php' );                // Plugins
        //remove_menu_page( 'users.php' );                  // Users
        //remove_menu_page( 'tools.php' );                  // Tools
        //remove_menu_page( 'options-general.php' );        // Settings

        // remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Ftheme-editor.php' );
        remove_submenu_page( 'options-general.php', 'options-media.php' );
    }
    // add_action( 'admin_menu', 'remove_menus' );



/* @_08 - Remove Admin bar in front
/ ================================================== */
    function remove_admin_bar() {
        return false;
    }
    add_filter('show_admin_bar', 'remove_admin_bar');

    // Add a toolbox widget in WordPress Dashboard
    function toolbox_dashboard_widget_function() {

        echo "<p>". __('Retrouvez-ici les documents mis à votre disposition :', DOMAIN)  ."</p>
                 <ul>
                    <li>&bullet; <a href=\"#TODO#\" target=\"_blank\">". __("Manuel d'utilisation du site et Google Analytics (PDF)", DOMAIN)  ."</a></li>
                    <li>&bullet; <a href=\"#TODO#\" target=\"_blank\">". __("Bon de livraison", DOMAIN)  ."</a></li>
                </ul>";
    }

    function wpc_add_dashboard_widgets() {
        wp_add_dashboard_widget('wp_dashboard_widget', 'Aloïs Petit', 'wpc_dashboard_widget_function');
        // wp_add_dashboard_widget('wp_toolbox_widget', __('Boite à outils', DOMAIN), 'toolbox_dashboard_widget_function');
    }
    add_action('wp_dashboard_setup', 'wpc_add_dashboard_widgets' );



/* @_10 - Hiding widgets in WordPress Dashboard
/ ================================================== */
    function wpc_dashboard_widgets() {
        global $wp_meta_boxes;
        // Last comments
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
        // Incoming links
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
        // Plugins
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
        // Plugins
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
        // Blog WordPress
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
        // Autres actualites
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    }
    // add_action('wp_dashboard_setup', 'wpc_dashboard_widgets');

    // Remove YOAST columns from admin list screens
    // add_filter( 'wpseo_use_page_analysis', '__return_false' );


/* @_11 - Editor style
/ ================================================== */
    add_editor_style( 'css/app-editor.css' );
