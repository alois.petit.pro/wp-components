export default class Home {

    /** message envoyé à l'utilisateur lors de sa première connexion */
    static message = "Salut toi, comment tu vas ?";

    /** boolean */
    static messageSend;

    constructor() {
        Home.messageSend = Home.getLocalStorage('messageSend');
        Home.sendMessage();
    }

    /** get localStorage value */
    static getLocalStorage(item) {
        return localStorage.getItem(item);
    }

    /** envoie un message à l'utilisateur */
    static sendMessage() {
        if(!Home.messageSend) {
            localStorage.setItem('messageSend', true);
            $(document).popup(Home.message);
        }
    }
}
