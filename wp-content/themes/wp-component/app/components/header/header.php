<header class="header">

    <?php if(isset($args['image']) && !empty($args['image'])): ?>
        <div class="header-thumbnail">
            <img src="<?= get_template_directory_uri() . $args['image'] ?>" alt="">
        </div>
    <?php endif; ?>

    <div class="header-container">

        <nav class="header-nav">
            <?php
                wp_nav_menu(array(
                    'theme_location' => $args['menu-name'],
                    'menu_class' => 'menu',
                    'container' => false
                ));
            ?>
        </nav>

    </div>

    <div class="header-title">
        <?php
            get_template_part('app/components/title/title', 'title', array(
                'level' => 1,
                'text' => $args['title']
            ));
        ?>
    </div>

</header>
