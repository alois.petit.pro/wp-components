<?php
/**
* Created by Aloïs Petit
* url: www.alois-petit.fr
*
* @ 00 - Domain
* @ 01 - Admin
* @ 02 - Init
*
*/

/* @ 00 - Domain
/* ============================================= */
define('DOMAIN', 'aloispetit');

/* @ 01 - Admin-wp
/* ============================================= */
get_template_part('admin/wpc-admin');

/* @ 02 - Init
/* ============================================= */
get_template_part('admin-settings/wpc-init');
