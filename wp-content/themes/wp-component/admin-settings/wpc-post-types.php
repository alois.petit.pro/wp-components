<?php
/**
* Created by Aloïs Petit
* User : Aloïs Petit
*
* @ 01 - Déclaration Custom post types
*
*/

/* @ 01 - Déclaration Custom post types
/* ============================================= */

function wpc_post_types()
{
    // projets
    register_post_type(
        'projet',
        array(
            'label'           => 'projet',
            'labels'          => array(
                'name'               => "projet",
                'singular_name'      => "projet",
                'all_items'          => "projets",
                'add_new_item'       => "Ajouter",
                'edit_item'          => "Éditer",
                'new_item'           => "Nouveau",
                'view_item'          => "Voir",
                'search_items'       => "Rechercher",
                'not_found'          => "Aucun élément trouvé",
                'not_found_in_trash' => "Aucun élément dans la corbeille",
            ),
            'public'          => true,
            'capability_type' => 'post',
            'supports'        => array(
                'title',
                'editor',
                'thumbnail',
            ),
            'menu_icon' => 'dashicons-screenoptions',
            'show_in_rest' => true,
            'has_archive'     => false,
        )
    );

}
add_action( 'init', 'wpc_post_types' );
