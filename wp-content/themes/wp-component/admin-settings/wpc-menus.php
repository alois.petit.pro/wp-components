<?php
/**
 * Created by Aloïs Petit
 * User : Aloïs Petit
 *
 * @ 01 - Menu navigation positions
 *
 */

/* @ 01 - Menu navigation positions
/* ============================================= */
register_nav_menus(array(
    'menu-primary' => __('Navigation principal'),
));
