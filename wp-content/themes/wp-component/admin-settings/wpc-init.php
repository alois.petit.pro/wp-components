<?php
/**
 * Created by Aloïs Petit
 * User : Aloïs Petit
 *
 * @ 00 - wpc_defines
 * @ 01 - wpc_styles
 * @ 02 - wpc_scripts
 * @ 03 - wpc_post_types
 * @ 05 - wpc_menus
 *
 */

 /* @ 00 - wpc_defines
 /* ============================================= */
get_template_part('admin-settings/wpc-defines');

 /* @ 01 - wpc_styles
 /* ============================================= */
get_template_part('admin-settings/wpc-styles');

/* @ 02 - wpc_scripts
/* ============================================= */
get_template_part('admin-settings/wpc-scripts');

/* @ 03 - wpc_post_types
/* ============================================= */
get_template_part('admin-settings/wpc-post-types');

/* @ 04 - wpc_menus
/* ============================================= */
get_template_part('admin-settings/wpc-menus');
