<?php
/**
 * Created by Aloïs Petit
 * User : Aloïs Petit
 *
 * @ 01 - Chargement Scripts
 *
 */

/* @ 01 - Chargement Scripts
/* ============================================= */
    function wpc_scripts()
    {
        // Scripts
        wp_enqueue_script('wpc-script', get_template_directory_uri() . "/assets/script/app.js", array('jquery'), '', true);

        // librairies externes
            // JQuery
            wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', '', '', false);

            // font awesome
            //wp_enqueue_script('font-awesome', 'https://kit.fontawesome.com/70ec0f88b5.js', '', '', false);

    }
    add_action('wp_enqueue_scripts', 'wpc_scripts'); // Add Theme Stylesheet
