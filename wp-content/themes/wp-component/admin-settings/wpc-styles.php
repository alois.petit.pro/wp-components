<?php
/**
 * Created by Aloïs Petit
 * User : Aloïs Petit
 *
 * @ 01 - Chargement Stylesheet
 *
 */

/* @ 01 - Chargement Stylesheet
/* ============================================= */
    function wpc_styles()
    {
        // Librairies

        // Fonts
        wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css2?family=Work+Sans:wght@200;400;500;600&display=swap', false);

        // Stylesheets
        wp_enqueue_style('styles', get_template_directory_uri() . '/assets/global/app.css', false);
    }
    add_action('wp_enqueue_scripts', 'wpc_styles');
