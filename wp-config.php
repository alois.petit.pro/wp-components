<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-component' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-1l]+Mwu|zkQf7aKV6a:D ?BEjVqoLRuf`U%D*u$c(6jec~`<b&!S-vd>G(c`@WB' );
define( 'SECURE_AUTH_KEY',  '-qwHmygG#&vlQF&DTzIlsLlIf[1nU.@g1 sWXId?W7jXGk5Ul)YMx |Vb*Ea^Te$' );
define( 'LOGGED_IN_KEY',    'qcQM`#,<cy/2}<L{]}45LJ`XURe/91n3+G-H7$J2bGXwCsX:<_g*]^I~#__I0tV|' );
define( 'NONCE_KEY',        ';BvB(DDlLdPp*eoz2w9=j$o#u@-]aLce2foYSR_l}FQZ]s.@[om^-m @$|Qe !R=' );
define( 'AUTH_SALT',        'Z-) nw#|hj{Pry)u:?PVJ57=k/ER}q>)1I&f%]<$`d:h uWiepk _Ck{/Y@_g_@]' );
define( 'SECURE_AUTH_SALT', '._iQo6K2/v_*IRjc&tj]6V[n}`)zX8yOBnO~8Vh?Ia&#kY~>tcFF#jI]TA1@jW g' );
define( 'LOGGED_IN_SALT',   'K#2z`{|>H@=?4a(MwS!VqR+}H_{5j}~v7DDj<g]Te$*oWB{ao&%w^)a}&4]L>Gk{' );
define( 'NONCE_SALT',       'VHHawW<zYbk1@a6wqL)u^QWuQ5?X,%O<OP{T/kC(j.sMW-]8(ZWoI[u5l S=Qlor' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
